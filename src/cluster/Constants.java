/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluster;
import java.util.LinkedList;
import movierecommender.imdbMovie;
import org.tartarus.snowball.ext.englishStemmer;
/**
 *
 * @author MAHE
 */
public class Constants 
{
    static String stopwords[]={"the","a","an","as","of","in","and","on","to","why","when","where","how","for",
        "at","by","with","this","he","she","his","her","there","has","video","say","from","new","be",
        "are","that","it","was","is"};
    static char symbols[]={'?','.','!',':','&',',','"','|'};

    
 
    
    static englishStemmer stemmer = new englishStemmer();
    //static Users user=new Users();
    
    static LinkedList<imdbMovie> clusters=new LinkedList();
    

    public static void stemUserKeys(String userKeywords)
    {
        String temp[]=getRoots(userKeywords);
        //userKeywords = temp;
    }
    
    public static String[] getRoots(String raw)
    {
        return getRoots(raw.split("[ ]"));
    }
    public static String[] getRoots(String[] words)
    {      
        String roots="";
        for(String stem:words)
        {
            stemmer.setCurrent(stem);
            if (stemmer.stem())
            {
                String root=stemmer.getCurrent();
                if(roots.contains(root)) ;
                else 
                    roots+=root.toLowerCase()+" ";                    
            }
        }
        return roots.split("[ ]");
    }
    public static String[] getRoots(LinkedList<String> raw)
    {       
        String roots="";
        for(String stem:raw)
        {
            stemmer.setCurrent(stem);
            if (stemmer.stem())
            {
                String root=stemmer.getCurrent();
                if(roots.contains(root)) ;
                else 
                    roots+=root+" ";                    
            }
        }
        return roots.split("[ ]");
    }
}
