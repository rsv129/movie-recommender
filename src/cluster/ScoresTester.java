/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluster;

import java.util.LinkedList;

/**
 *
 * @author ravi
 */
public class ScoresTester 
{
    public static void main(String args[])
    {
        String str1="In turn of the century Vienna, a magician uses his abilities to secure the love of a woman far above his social standing.";
        String str2="After a tragic accident two stage magicians engage in a battle to create the ultimate illusion whilst sacrificing everything they have to outwit the other.";
//        System.out.println(str1);
//        System.out.println(str2);
//        LinkedList<VectorData> matrix=Scores.constructVectorMatrix(str1.split("[ ]"), str2.split("[ ]"));
//        System.out.println("Generated Vector:");
//        System.out.println("Word\tA\tB");
//        for(VectorData x:matrix)
//        {
//            x.print();
//        }
//        double dice=Scores.dice(matrix);
//        double cosine=Scores.cosine(matrix);
//        System.out.println();
//        System.out.println("Dice: "+dice);  //Should be 76%
//        System.out.println("Cosine: "+cosine); //Should be 82%
        Scorer s = new Scorer(str1,str2);
        s.calc();
    }
}
