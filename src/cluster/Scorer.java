/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluster;

import java.util.LinkedList;

/**
 *
 * @author ravi
 */
public class Scorer {
    
    double cosine;
    double dice;
    String str1;
    String str2;
    
    
    public Scorer(String one , String two)
    {
        str1 = one;
        str2 = two;
    }
    
    public Scorer()
    {
        
    }
    
    public void setStrings(String one , String two)
    {
        str1 = one;
        str2 = two;
    }
    
    
    public void calc()
    {
        //System.out.println(str1);
        //System.out.println(str2);
        LinkedList<VectorData> matrix=Scores.constructVectorMatrix(str1.split("[ ]"), str2.split("[ ]"));
        //System.out.println("Generated Vector:");
        //System.out.println("Word\tA\tB");
//        for(VectorData x:matrix)
//        {
//            x.print();
//        }
        dice=Scores.dice(matrix);
        cosine=Scores.cosine(matrix);
//        System.out.println();
//        System.out.println("Dice: "+dice);  //Should be 76%
//        System.out.println("Cosine: "+cosine); //Should be 82%
    }
    
    
    public double getCosine()
    {
        return cosine;
    }
    
    public double getDice()
    {
        return dice;
    }
    
}
