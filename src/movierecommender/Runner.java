/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movierecommender;

import java.io.IOException;
import cluster.*;
import java.util.LinkedList;
import org.json.*;

/**
 *
 * @author ravi
 */
public class Runner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        imdbMovie userMovie = new imdbMovie();
        String query = "http://theapache64.xyz:8080/movie_db/search?keyword=Transporter";
        String input = "";
//        for(int i=0 ; i<args.length ; i++)
//        {
//            input+=args[i];
//            input+="_";
//        }
    //    input = input.substring(0, (input.length()-1));
    //    System.out.println(input);
    //    query+=input;
        LinkedList <imdbMovie> movieList = new LinkedList();
        LinkedList <imdbMovie> genreMovies = new LinkedList();
        JSONParser jp = new JSONParser(query,userMovie);
        jp.getJson();
        String csvFile = "/home/ravi/NetBeansProjects/MovieRecommender/csv/imdb5000.csv";
        CSVParser parser = new CSVParser(csvFile, userMovie);
        movieList = parser.parse();
        genreCluster clusterer = new genreCluster(movieList , userMovie);
        genreMovies = clusterer.cluster();
        Scorer scorer = new Scorer();
        for(int i = 0 ; i<genreMovies.size() ; i++)
        {
            scorer.setStrings(userMovie.plot, genreMovies.get(i).plot);
            scorer.calc();
            genreMovies.get(i).cosine = scorer.getCosine();
            genreMovies.get(i).dice = scorer.getDice();
            //System.out.println("Cosine : "+genreMovies.get(i).cosine+" Dice : "+genreMovies.get(i).dice);
        }
        
        for(int i=0 ; i<genreMovies.size() ; i++)
        {
            for(int j=0 ; j<genreMovies.size() ; j++)
            {
                if(genreMovies.get(i).cosine>genreMovies.get(j).cosine)
                {
                    imdbMovie temp = genreMovies.get(i);
                    genreMovies.set(i, genreMovies.get(j));
                    genreMovies.set(j, temp);
                }
            }
        }
        
        for(int i=0 ; i<20 ; i++)
        {
            System.out.println(genreMovies.get(i).name + " : Cosine = "+genreMovies.get(i).cosine);
        }
        
    }
}
