/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movierecommender;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ravi
 */
public class imdbMovie 
{
    String name;
    float rating;
    public String plot;
    public HashMap <String , Boolean > genres;
    public String genre[];
    double cosine;
    double dice;
    
    public imdbMovie(String n , float r , String p)
    {
        name = n;
        plot = p;
        rating = r;
        genres = new HashMap<>();
    }
    
    public void imdbMovieInit (String n , float r , String p)
    {
        name = n;
        plot = p;
        rating = r;
        genres = new HashMap<>();
    }
    
    public imdbMovie() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        genres = new HashMap<>();
    }
    
    void addGenres(String gen)
    {
        
        genre = gen.split(",");
        for(int i=0 ; i<genre.length ; i++)
            genres.put(genre[i], Boolean.TRUE);
        
    }
}
