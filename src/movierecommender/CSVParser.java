/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movierecommender;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author ravi
 */
public class CSVParser {
    
    String csvFile;
    imdbMovie userMovie;
    LinkedList<imdbMovie> movieSet;
    
    CSVParser(String item , imdbMovie user)
    {
        csvFile = item;
        userMovie = user;
        movieSet = new LinkedList();
    }

    String genreParser(String genres)
    {
        ArrayList<String> generatedGenres = new ArrayList();
        String returnString = new String();
        int colonArray[] = new int[100];
        int colonCount = -1;
        for(int i=0 ; i<genres.length() ; i++)
        {
            if(genres.charAt(i)==':')
                colonArray[++colonCount] = i;
        }
        for(int i=1,j=0 ; i<=colonCount ; i=i+2,j++)
        {
            int k=genres.indexOf("}", colonArray[i]);
            generatedGenres.add(genres.substring(colonArray[i]+2, k));      
        }
        for(int i=0 ; i<generatedGenres.size() ; i++)
        {
            String toadd = generatedGenres.get(i).replace("\"\"","");
            returnString += toadd;
            returnString += ",";
            //System.out.println(generatedGenres.get(i));
        }
        //System.out.println(returnString);
        return returnString;
    }
    
    
    LinkedList<imdbMovie> parse()
    {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            int k=0;
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] items  = line.split(cvsSplitBy);
                imdbMovie movie;
                String name = new String();
                String plot = new String();
                float rating = 0;
                String genres = new String();
                for(int i=0 ; i<items.length ; i++)
                {
                    
                    if(i==1)
                        genres = genreParser(items[i]);
                    if(i==6)
                    {    
                        //System.out.println("Title : "+items[i]);
                        name = items[i];
                    }
                    if(i==7)
                    {
                        //System.out.println("Plot : "+items[i]);
                        plot = items[i];
                    }
                    if(i==18)
                    {
                        if(items[i].length()==3)
                            rating = (float) Float.parseFloat(items[i]);
                    }
                }
                movie = new imdbMovie(name , rating , plot);
                movie.addGenres(genres);
                movieSet.add(movie);
                //System.out.println("New MOVIE");

            }
            System.out.println("Array size is "+ movieSet.size());
//            for(int i=0 ; i<=movieSet.size() ; i++)
//            {
//                System.out.println(movieSet.get(i).name);
//                System.out.println(movieSet.get(i).plot);
//                System.out.println(movieSet.get(i).genres);
//                System.out.println(movieSet.get(i).rating);
//            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return movieSet;
    }
    
}
